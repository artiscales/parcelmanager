package fr.ign.artiscales.pm.parcel;

import fr.ign.artiscales.tools.geoToolsFunctions.Attribute;
import fr.ign.artiscales.tools.geoToolsFunctions.SchemaProp;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SyntheticParcel {

    private static int idGen = 0;
    public Geometry geom;
    public String id;
    public double area, distanceToCenter;
    public long nbNeighborhood;
    public int ownerID, regionID;
    public List<String> lIdNeighborhood;

    public SyntheticParcel(Geometry geom, double area, double distanceToCenter, long nbNeighborhood, int ownerID, int regionID) {
        this.id = String.valueOf(idGen++);
//        this.id = "Parcel" + Attribute.makeUniqueId();
        this.geom = geom;
        this.area = area;
        this.distanceToCenter = distanceToCenter;
        this.nbNeighborhood = nbNeighborhood;
        this.ownerID = ownerID;
        this.regionID = regionID;
    }

    public SyntheticParcel(String id, Geometry geom, double area, double distanceToCenter, long nbNeighborhood, int ownerID, int regionID) {
        this.id = id;
        this.geom = geom;
        this.area = area;
        this.distanceToCenter = distanceToCenter;
        this.nbNeighborhood = nbNeighborhood;
        this.ownerID = ownerID;
        this.regionID = regionID;
    }

    /**
     * create the distribution of the sum of owned land
     *
     * @param lSP analyzed synthetic parcels
     * @return unordered sums of area
     */
    public static List<Double> sumOwnerOwnedArea(List<SyntheticParcel> lSP) {
        HashMap<Integer, Double> ow = new HashMap<>();
        for (SyntheticParcel sp : lSP)
            if (ow.containsKey(sp.ownerID))
                ow.put(sp.ownerID, sp.area + ow.get(sp.ownerID));
            else
                ow.put(sp.ownerID, sp.area);
        return new ArrayList<>(ow.values());
    }

    /**
     * Export to a geographic file.
     * @param lSP List of synthetic parcels to export
     * @param outFile name of exported file. Geopackage by default, but if file name ends with .shp or .geojson, export will be in this format.
     * @throws IOException writing file
     */
    public static void export(List<SyntheticParcel> lSP, File outFile) throws IOException {
        DefaultFeatureCollection result = new DefaultFeatureCollection();
        SimpleFeatureBuilder sfb = getSFB();
        for (SyntheticParcel sp : lSP) {
            sfb.set(CollecMgmt.getDefaultGeomName(), sp.geom);
            sfb.set("nbNeighborhood", sp.nbNeighborhood);
            sfb.set("idParcelNeighborhood", String.join("---", sp.lIdNeighborhood));
            sfb.set("area", sp.area);
            sfb.set("distanceToCenter", sp.distanceToCenter);
            sfb.set("parcelID", sp.id);
            sfb.set("ownerID", sp.ownerID);
            sfb.set("regionID", sp.regionID);
            result.add(sfb.buildFeature(Attribute.makeUniqueId()));
        }
        CollecMgmt.exportSFC(result, outFile);
    }

    private static SimpleFeatureBuilder getSFB() {
        SimpleFeatureTypeBuilder sfTypeBuilder = new SimpleFeatureTypeBuilder();
        try {
            sfTypeBuilder.setCRS(CRS.decode(SchemaProp.getEpsg()));
        } catch (FactoryException e) {
            e.printStackTrace();
        }
        sfTypeBuilder.setName("SyntheticParcel");
        sfTypeBuilder.add(CollecMgmt.getDefaultGeomName(), Polygon.class);
        sfTypeBuilder.add("nbNeighborhood", Long.class);
        sfTypeBuilder.add("idParcelNeighborhood", String.class);
        sfTypeBuilder.add("area", Double.class);
        sfTypeBuilder.add("distanceToCenter", Double.class);
        sfTypeBuilder.add("parcelID", String.class);
        sfTypeBuilder.add("ownerID", Integer.class);
        sfTypeBuilder.add("regionID", Integer.class);
        sfTypeBuilder.setDefaultGeometry(CollecMgmt.getDefaultGeomName());
        SimpleFeatureType featureType = sfTypeBuilder.buildFeatureType();
        return new SimpleFeatureBuilder(featureType);
    }

    public static void resetIdParcels() {
        idGen=0;
    }

    public static List<SyntheticParcel> convertToSyntheticParcel(SimpleFeatureCollection cutParcels) {
        List<SyntheticParcel> lSP = new ArrayList<>(cutParcels.size());
        try(SimpleFeatureIterator it = cutParcels.features()){
            while (it.hasNext()){
                SimpleFeature gtParcel = it.next();
                lSP.add(new SyntheticParcel((Geometry) gtParcel.getDefaultGeometry(), ((Geometry) gtParcel.getDefaultGeometry()).getArea(),
                0, 0, 0, 0));
            }
        }
        return lSP;
    }

    public void setIdNeighborhood(List<SyntheticParcel> lSP) {
        lIdNeighborhood = lSP.stream().filter(g -> g.geom.isWithinDistance(geom, 1)).filter(g -> !g.geom.equals(geom)).map(sp -> sp.id).collect(Collectors.toList());
    }
}
