# Version 1.3
OpenMole ready version. 
## ADD
* Synthetic parcels
* Random seed as a parameter

## FIX
* Multiple error cases corrected in Straight Skeleton processes
* Bug in indicators : neighborhood were always including the parcel itself as a neighboor

## Technical changes
* Java could be downgraded to version 11 in order to be put in OpenMole
* pom cleaned and code deployed into INRIA's servers



# Version 1.2
Version submited in Colomb & al. revision

## ADD
  * More selection options in scenarios  

## FIX

  * Perf improved
  * Fix densOrNeighborhood workflow and replace them in decomposed tasks in current useCases 
  * correct a bug on OBB+SS

# Version 1.1.1
## FIX

  * Report attribute simplification to scenarios. Fix an attribute bug in ZoneDivision workflow.
  
# Version 1.1

## ADD
  * Add the OBBThenSS process.
  * Update for OpenMole calibration task.
  * Add possibility for workflows to process FlagDivision.

## FIX

  * Simplify the attribute management (cf. <a href="src/main/resources/doc/AttributePolicy.md">this updated documentation</a>).
  * Rewrite FlagParcel process.
  * Add <i>harmonyCoeff</i> and <i>noise</i> parameters to possibly be set everywhere.


# Version 1.0 : 

First relase for Colomb21 article.